import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './Tugas/Tugas13/loginScreen'
import Register from './Tugas/Tugas13/registerScreen'
import About from './Tugas/Tugas13/aboutScreen'
import App2 from './Tugas/Tugas14/App'
import FlatList from './Latihan/flatList'
import Skill from './Tugas/Tugas14/skillScreen'
import Navigator from './Latihan/index'
import Index from './Tugas/Tugas15/index'

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Login />
      <Register /> */}
      {/* <About /> */}
      {/* <App2 /> */}
      {/* <FlatList /> */}
      {/* <Skill /> */}
      {/* <Navigator /> */}
      <Index />
      {/* <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
});
