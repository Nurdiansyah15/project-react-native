import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native'

export default class Register extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Image style={styles.img} source={require("./images/logo.png")} />
        <View style={styles.textBox}>
          <Text style={styles.textLogin}>Register</Text>
        </View>


        <Text style={styles.label}>Username</Text>
        <TextInput style={styles.textInput} />
        <Text style={styles.label2}>Email</Text>
        <TextInput style={styles.textInput2} />
        <Text style={styles.label3}>Password</Text>
        <TextInput style={styles.textInput3} />
        <Text style={styles.label4}>Ulangi Password</Text>
        <TextInput style={styles.textInput4} />




        <TouchableOpacity style={styles.masuk}>
          <Text style={styles.textMasuk}>Daftar</Text>
        </TouchableOpacity>
        <View style={styles.atau}>
          <Text style={styles.textAtau}>atau</Text>
        </View>
        <TouchableOpacity style={styles.daftar}>
          <Text style={styles.textMasuk}>Masuk ?</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  atau: {
    position: 'absolute',
    width: 48,
    height: 28,
    left: 164,
    top: 731,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  daftar: {
    position: 'absolute',
    width: 140,
    height: 40,
    left: 118,
    top: 775,
    backgroundColor: '#003366',
    borderRadius: 16,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 100
  },
  textMasuk: {
    fontSize: 20,
    color: 'white'
  },
  masuk: {
    position: 'absolute',
    width: 140,
    height: 40,
    left: 118,
    top: 675,
    backgroundColor: '#3EC6FF',
    borderRadius: 16,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    fontFamily: 'Roboto',
  },
  img: {
    height: 102,
    width: 375,
    marginTop: 63
  },
  textLogin: {
    fontSize: 24,
    lineHeight: 28,
    color: '#003366'
  },
  textBox: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 70,

  },
  textInput: {
    width: 294,
    height: 48,
    background: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#003366',
    position: 'absolute',
    left: 41,
    top: 326
  },
  label: {
    position: 'absolute',
    width: 127,
    height: 19,
    left: 41,
    top: 303,
    fontSize: 16,
    color: '#003366'
  },
  label2: {
    position: 'absolute',
    width: 127,
    height: 19,
    fontSize: 16,
    left: 41,
    top: 390,
    color: '#003366'
  },
  label3: {
    position: 'absolute',
    width: 127,
    height: 19,
    fontSize: 16,
    left: 41,
    top: 477,
    color: '#003366'
  },
  label4: {
    position: 'absolute',
    width: 127,
    height: 19,
    fontSize: 16,
    left: 41,
    top: 564,
    color: '#003366'
  },
  textInput2: {
    width: 294,
    height: 48,
    background: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#003366',
    position: 'absolute',
    left: 41,
    top: 413
  },
  textInput3: {
    width: 294,
    height: 48,
    background: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#003366',
    position: 'absolute',
    left: 41,
    top: 500
  },
  textInput4: {
    width: 294,
    height: 48,
    background: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#003366',
    position: 'absolute',
    left: 41,
    top: 587
  },
})