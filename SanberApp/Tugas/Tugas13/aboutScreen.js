import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Image,
  TouchableOpacity
} from 'react-native'

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Icon name="rocket" size={30} color="#900" /> */}
        <Text style={styles.title}>Tentang Saya</Text>
        <Icon style={styles.icon} name='user-circle' size={100} />
        <Text style={styles.name}>Nurdiansyah</Text>
        <Text style={styles.job}>React Native Developer</Text>
        <View style={styles.portofolio}>
          <Text style={{ fontSize: 18 }}>Portofolio</Text>
          <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'spaceBetween' }}>
            <View style={styles.itemPortofolio}>
              <Icon name='gitlab' size={40} style={{ color: '#3EC6FF' }} />
              <Text>@Nurdiansyah15</Text>
            </View>
            <View style={styles.itemPortofolio}>
              <Icon name='github' size={40} style={{ color: '#3EC6FF' }} />
              <Text>@Nurdiansyah15</Text>
            </View>
          </View>
        </View>
        <View style={styles.contact}>
          <Text style={{ fontSize: 18 }}>Hubungi Saya</Text>
          <View style={{ display: 'flex', flexDirection: 'col', alignItems: 'center', marginTop: 10, height: 200, padding: 15 }}>
            <View style={styles.itemContact}>
              <Icon name='facebook-square' size={40} style={{ marginRight: 10, color: '#3EC6FF' }} />
              <Text>Nurdiansyah</Text>
            </View>
            <View style={styles.itemContact}>
              <Icon name='instagram' size={40} style={{ marginRight: 10, color: '#3EC6FF' }} />
              <Text>@mr_nurdiansyah</Text>
            </View>
            <View style={styles.itemContact}>
              <Icon name='twitter' size={40} style={{ marginRight: 10, color: '#3EC6FF' }} />
              <Text>@mrnurd150900</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemContact: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20
  },
  itemPortofolio: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
    width: 180,
    height: 80
  },
  contact: {
    height: 250,
    width: 360,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    padding: 10

  },
  portofolio: {
    padding: 10,
    height: 140,
    width: 360,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    marginTop: 16,
    marginBottom: 9,
  },
  job: {
    marginTop: 8,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#3EC6FF',

  },
  name: {
    marginTop: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 24,
    color: '#003366',
  },
  title: {
    marginTop: 64,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 36,
    color: '#003366',
  },
  icon: {
    fontSize: 200,
    color: '#EFEFEF',
    textAlign: 'center',
    marginTop: 12
  },
  container: {
    flex: 1,
    fontFamily: 'Roboto',
  },
})