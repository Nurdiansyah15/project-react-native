import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Image,
  TouchableOpacity
} from 'react-native'

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.img} source={require("./images/logo.png")} />
        <View style={styles.textBox}>
          <Text style={styles.textLogin}>Login</Text>
        </View>
        <Text style={styles.username}>Username/Email</Text>
        <TextInput style={styles.textInput} />
        <Text style={styles.password}>Password</Text>
        <TextInput style={styles.textInput2} />
        <TouchableOpacity style={styles.masuk}>
          <Text style={styles.textMasuk}>Masuk</Text>
        </TouchableOpacity>
        <View style={styles.atau}>
          <Text style={styles.textAtau}>atau</Text>
        </View>
        <TouchableOpacity style={styles.daftar}>
          <Text style={styles.textMasuk}>Daftar ?</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  atau: {
    position: 'absolute',
    width: 48,
    height: 28,
    left: 164,
    top: 549,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  daftar: {
    position: 'absolute',
    width: 140,
    height: 40,
    left: 118,
    top: 593,
    backgroundColor: '#003366',
    borderRadius: 16,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textMasuk: {
    fontSize: 20,
    color: 'white'
  },
  masuk: {
    position: 'absolute',
    width: 140,
    height: 40,
    left: 118,
    top: 493,
    backgroundColor: '#3EC6FF',
    borderRadius: 16,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    fontFamily: 'Roboto',
  },
  img: {
    height: 102,
    width: 375,
    marginTop: 63
  },
  textLogin: {
    fontSize: 24,
    lineHeight: 28,
    color: '#003366'
  },
  textBox: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 70,

  },
  textInput: {
    width: 294,
    height: 48,
    background: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#003366',
    position: 'absolute',
    left: 41,
    top: 326
  },
  username: {
    position: 'absolute',
    width: 127,
    height: 19,
    left: 41,
    top: 303,
    fontSize: 16,
    color: '#003366'
  },
  password: {
    position: 'absolute',
    width: 127,
    height: 19,
    fontSize: 16,
    left: 41,
    top: 390,
    color: '#003366'

  },
  textInput2: {
    width: 294,
    height: 48,
    background: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#003366',
    position: 'absolute',
    left: 41,
    top: 413
  }
})