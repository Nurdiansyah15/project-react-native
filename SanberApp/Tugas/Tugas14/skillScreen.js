import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native'
import data from './skillData.json'

const dataList = data
const extractKey = ({ id }) => id
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { AntDesign } from '@expo/vector-icons'


export default class Skill extends Component {
  renderItem = ({ item }) => {
    return (
      <View style={styles.containerItem}>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: 100 }}>
          <MaterialCommunityIcons name={item.iconName} style={styles.icon2} />
        </View>
        <View style={{ display: 'flex', flexDirection: 'col', justifyContent: 'center', width: 180 }}>
          <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#003366' }}>{item.skillName}</Text>
          <Text style={{ fontSize: 16, color: '#3EC6FF' }}>{item.categoryName}</Text>
          <Text style={{ fontSize: 48, fontWeight: 'bold', color: 'white', display: 'inline-block', alignSelf: 'flex-end' }}>{item.percentageProgress}</Text>
        </View>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: 80 }}>
          <AntDesign name='rightcircle' style={styles.icon} />
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.boxImage}>
            <Image style={styles.img} source={require('../Tugas13/images/logo.png')} />
          </View>
          <View style={styles.akun}>
            <Icon style={{ fontSize: 25, color: '#3EC6FF', marginRight: 10, display: 'flex', alignItems: 'center' }} name='user-circle' />
            <View style={{ display: 'flex' }}>
              <Text style={{ fontSize: 12, color: 'black' }}>Hai,</Text>
              <Text style={{ fontSize: 16, color: '#003366' }}>Nurdiansyah</Text>
            </View>
          </View>
          <Text style={{ fontSize: 36, color: '#003366' }}>SKILL</Text>
        </View>
        <View style={styles.category}>
          <View style={styles.itemCategory}>
            <Text style={{ fontSize: 12, color: '#003366' }}>Library/Framework</Text>
          </View>
          <View style={styles.itemCategory}>
            <Text style={{ fontSize: 12, color: '#003366' }}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.itemCategory}>
            <Text style={{ fontSize: 12, color: '#003366' }}>Teknologi</Text>
          </View>
        </View>
        <FlatList
          style={{ marginTop: 10 }}
          data={dataList.items}
          renderItem={this.renderItem}
          keyExtractor={extractKey}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  icon: {
    fontSize: 40,

  },
  icon2: {
    fontSize: 80,

  },
  containerItem: {
    height: 130,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: 10
  },
  itemCategory: {
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#B4E9FF',
    borderRadius: 8
  },
  category: {
    alignSelf: 'stretch',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  header: {
    borderBottomWidth: 5,
    borderColor: '#3EC6FF'
  },
  boxImage: {
    alignSelf: 'stretch',
    height: 51,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  akun: {
    alignSelf: 'stretch',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  container: {
    flex: 1,
    fontFamily: 'Roboto',
    padding: 20
  },
  img: {
    width: 187.5,
    height: 51,
  },
})